# ASP.NET MVC + Entity Framework #

### Get Started ###

Command 

- Open package manager console
    - Enable-Migrations
    - Enable-Migrations -ContextTypeName pock_asp_net_mvc_ef.Models.Context

- Validar string de conexão (Web.config)

### Concepts ###

Migration:
    - Responsavel por pegar o código e converter em base de dados;
    - Configurável o automatic migration (Configuration.cs);

Gerar um script inicial para criar o contexto de banco de dados

Add-Migration Start : leitura do código verifica o que tem de diferente do arquivo migration e atualiza  