﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace pock_asp_net_mvc_ef.Models
{
    //Db Context tem a responsabilidade em mapear os models e as tableas
    public class Context : DbContext
    {
        public Context() : base(ConfigurationManager.ConnectionStrings["conexao"].ConnectionString)
        {

        }

        //Mapear a Entidade do Tipo Pessoa
        public DbSet<Pessoa> Pessoa { get; set; }

        //public DbSet<Endereco> Pessoa { get; set; }
    }
}