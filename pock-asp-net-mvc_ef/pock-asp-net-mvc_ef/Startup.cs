﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(pock_asp_net_mvc_ef.Startup))]
namespace pock_asp_net_mvc_ef
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
