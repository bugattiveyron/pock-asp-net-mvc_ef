﻿using System.Web;
using System.Web.Mvc;

namespace pock_asp_net_mvc_ef
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
